<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity s02</title>
</head>
<body>

<h1>Loops</h1>

	<h3>Divisible by Five</h3>
		<p><?php divisibleByFive() ?></p>

<h1>Arrays</h1>

	<h3>Array Manipulation</h3>
		<p><?php array_push($students, 'John Smith'); ?></p>
		<p>array(1) <?php print_r($students); ?></p>
		<p><?php echo count($students); ?></p>
		<p><?php array_push($students, 'Jane Smith'); ?></p>
		<p>array(2) <?php print_r($students); ?></p>
		<p><?php echo count($students); ?></p>
		<p><?php array_shift($students); ?></p>
		<p>array(1) <?php print_r($students); ?></p>
		<p><?php echo count($students); ?></p>

</body>
</html>